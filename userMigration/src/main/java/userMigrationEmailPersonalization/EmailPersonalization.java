package userMigrationEmailPersonalization;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class EmailPersonalization {

	public static void main(String[] args) {
		
		try
		{
			String folderPath = "D:\\Migration\\EmailPersonalization\\Latest DB Ids";
			String fileName = "Set1";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String sevenfilePath = folderPath+"\\SevenX\\"+fileName+".txt";
			String eightfilePath = folderPath+"\\EightX\\"+fileName+".txt";
			ArrayList<String> missingids = new ArrayList<String>(10000000);
			
			int maxLimit = 0;
			int recordsToLimit = 500000;
			System.out.println("----------------------------- 1 ------------------------");
			
			String file1Id = null;
			String file2Id = null;
			
			BufferedReader file1Reader = new BufferedReader(new FileReader(sevenfilePath));
			
			HashMap<String, String> sevenxAttrDetails = new HashMap<String, String>();
			HashMap<String, String> eightxAttrDetails = new HashMap<String, String>();
			
			LinkedHashMap<String, HashMap<String, String>> sevenxUserDetails = new LinkedHashMap<String, HashMap<String, String>>();
			LinkedHashMap<String, HashMap<String, String>> eightxUserDetails = new LinkedHashMap<String, HashMap<String, String>>();
			
			File userFailCreatedForSevenX = new File(folderPath+"\\userIdsFailedDuringCreationForSevenX.txt");
			FileWriter writer = new FileWriter(userFailCreatedForSevenX);
			
			int j=0;
			while((file1Id = file1Reader.readLine())!=null)
			{
				if(maxLimit>recordsToLimit)
					break;
				int currentCount = j%5000;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				String khid = null, userId = null, recentTags = null, emailPersonalizationFlag = null;
				String[] tempval = file1Id.split("\t");
				if(tempval.length>4)
				{
					khid = tempval[1];
					userId = tempval[2];
					emailPersonalizationFlag = tempval[3];
					recentTags = tempval[tempval.length-1];
				}
				else
				{
					if(userFailCreatedForSevenX.createNewFile())
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write(file1Id+"\r\n");
					}
					else
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write("\r\n");
						writer.write(file1Id+"\r\n");
					}
				}
				
				sevenxAttrDetails.put("EmailFlag", emailPersonalizationFlag);
				sevenxAttrDetails.put("KHID", khid);
				sevenxAttrDetails.put("RecentTag", recentTags);
				
				HashMap<String, String> tmp = new HashMap<String, String>();
				tmp.putAll(sevenxAttrDetails);
				sevenxUserDetails.put(userId, tmp);
				j++;
				maxLimit++;
				//sevenuserids.add(file1Id);
			}
			file1Reader.close();
			writer.close();
			
			System.out.println(sevenxUserDetails.size());
			
			System.out.println("1 Completed "+System.currentTimeMillis());
			System.out.println("----------------------------- 2 ------------------------");
			
			File userFailCreatedForEightX = new File(folderPath+"\\userIdsFailedDuringCreationForEightX.txt");
			writer = new FileWriter(userFailCreatedForEightX);
			
			j=0;
			maxLimit=0;
			BufferedReader file2Reader = new BufferedReader(new FileReader(eightfilePath));
			while((file2Id = file2Reader.readLine())!=null)
			{
				if(maxLimit>recordsToLimit)
					break;
				int currentCount = j%5000;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				String khid = null, userId = null, recentTags = null, emailPersonalizationFlag = null;
				String[] tempval = file2Id.split("\t");
				if(tempval.length>4)
				{
					khid = tempval[1];
					userId = tempval[2];
					emailPersonalizationFlag = tempval[3];
					recentTags = tempval[tempval.length-1];
				}
				else
				{
					if(userFailCreatedForEightX.createNewFile())
					{
						writer = new FileWriter(userFailCreatedForEightX,true);
						writer.write(file1Id+"\r\n");
					}
					else
					{
						writer = new FileWriter(userFailCreatedForEightX,true);
						writer.write("\r\n");
						writer.write(file1Id+"\r\n");
					}
				}
				
				eightxAttrDetails.put("EmailFlag", emailPersonalizationFlag);
				eightxAttrDetails.put("KHID", khid);
				eightxAttrDetails.put("RecentTag", recentTags);
				
				HashMap<String, String> tmp = new HashMap<String, String>();
				tmp.putAll(eightxAttrDetails);
				eightxUserDetails.put(userId, tmp);
				j++;
				maxLimit++;
				//sevenuserids.add(file1Id);
			}
			file2Reader.close();
			writer.close();
			
			System.out.println("2 Completed "+System.currentTimeMillis());
			
			System.out.println("----------------------------- 3 ------------------------");
			
			File failedUserIdepath = new File(folderPath+"\\MissingUserIds.txt");
			File dataMismatchUserIdepath = new File(folderPath+"\\DataMismatchUserIds.txt");
			
			j=0;
			for(Map.Entry<String, HashMap<String, String>> sevenxIds:sevenxUserDetails.entrySet())
			{
				int currentCount = j%5000;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				String sevenXkeyId = sevenxIds.getKey();
				HashMap<String, String> sevenXkeyValue = sevenxIds.getValue();
				boolean matchFound = false;
				boolean dataMismatch = false;
				if(eightxUserDetails.containsKey(sevenXkeyId))
				{
					matchFound = true;
					HashMap<String, String> eightXkeyValue = eightxUserDetails.get(sevenXkeyId);
					//System.out.println(eightXkeyValue);
					
					String sevenXKHIDValue = sevenXkeyValue.get("KHID");
					String sevenXEmailFlagValue = sevenXkeyValue.get("EmailFlag");
					//String sevenXRecentTagValue = sevenXkeyValue.get("RecentTag");
					
					String eightXKHIDValue = eightXkeyValue.get("KHID");
					String eightXEmailFlagValue = eightXkeyValue.get("EmailFlag");
					//String eightXRecentTagValue = eightXkeyValue.get("RecentTag");
					
					if(!sevenXKHIDValue.equalsIgnoreCase(eightXKHIDValue))
					{
						dataMismatch = true;
						if(dataMismatchUserIdepath.createNewFile())
						{
							writer = new FileWriter(dataMismatchUserIdepath,true);
							writer.write(sevenXkeyId+"\r\n");
							writer.write("==========="+"\r\n");
							writer.write("Expected Seven X KHID Values : "+ sevenXKHIDValue+"\r\n");
							writer.write("Actual Eight X KHID Values : "+ eightXKHIDValue+"\r\n");
							writer.write("==========="+"\r\n");
						}
						else
						{
							writer = new FileWriter(dataMismatchUserIdepath,true);
							writer.write(sevenXkeyId+"\r\n");
							writer.write("==========="+"\r\n");
							writer.write("Expected Seven X KHID Values : "+ sevenXKHIDValue+"\r\n");
							writer.write("Actual Eight X KHID Values : "+ eightXKHIDValue+"\r\n");
							writer.write("==========="+"\r\n");
						}
						
						writer.close();
					}
					
					if(!sevenXEmailFlagValue.equalsIgnoreCase(eightXEmailFlagValue))
					{
						if(dataMismatchUserIdepath.createNewFile())
						{
							writer = new FileWriter(dataMismatchUserIdepath,true);
							if(!dataMismatch)
							{
								writer.write(sevenXkeyId+"\r\n");
								writer.write("==========="+"\r\n");
							}
							writer.write("Expected Seven X Email Values : "+ sevenXEmailFlagValue+"\r\n");
							writer.write("Actual Eight X Email Values : "+ eightXEmailFlagValue+"\r\n");
							writer.write("==========="+"\r\n");
						}
						else
						{
							writer = new FileWriter(dataMismatchUserIdepath,true);
							if(!dataMismatch)
							{
								writer.write(sevenXkeyId+"\r\n");
								writer.write("==========="+"\r\n");
							}
							writer.write("Expected Seven X Email Values : "+ sevenXEmailFlagValue+"\r\n");
							writer.write("Actual Eight X Email Values : "+ eightXEmailFlagValue+"\r\n");
							writer.write("==========="+"\r\n");
						}
						dataMismatch = true;
						writer.close();
					}
					
					/*if(!sevenXRecentTagValue.equalsIgnoreCase(eightXRecentTagValue))
					{
						if(dataMismatchUserIdepath.createNewFile())
						{
							writer = new FileWriter(dataMismatchUserIdepath,true);
							if(!dataMismatch)
							{
								writer.write(sevenXkeyId+"\r\n");
								writer.write("==========="+"\r\n");
							}
							writer.write("Expected Seven X Recent Tag Values : "+ sevenXRecentTagValue+"\r\n");
							writer.write("Actual Eight X Recent Tag Values : "+ eightXRecentTagValue+"\r\n");
							writer.write("==========="+"\r\n");
						}
						else
						{
							writer = new FileWriter(dataMismatchUserIdepath,true);
							if(!dataMismatch)
							{
								writer.write(sevenXkeyId+"\r\n");
								writer.write("==========="+"\r\n");
							}
							writer.write("Expected Seven X Recent Tag Values : "+ sevenXRecentTagValue+"\r\n");
							writer.write("Actual Eight X Recent Tag Values : "+ eightXRecentTagValue+"\r\n");
							writer.write("==========="+"\r\n");
						}
						dataMismatch = true;
						writer.close();
					}*/
				}
				
				/*for(Map.Entry<String, HashMap<String, String>> eightxIds:eightxUserDetails.entrySet())
				{
					String eightXkeyId = eightxIds.getKey();
					if(sevenXkeyId.equalsIgnoreCase(eightXkeyId))
					{
						//System.out.println("Match Found");
						matchFound = true;
						break;
					}
				}*/
				
				if(!matchFound)
				{
					missingids.add(sevenXkeyId);
				}
				j++;
			}
			
			if(failedUserIdepath.createNewFile())
			{
				writer = new FileWriter(failedUserIdepath,true);
				for(String ids:missingids)
				{
					writer.write(ids+"\r\n");
				}
			}
			else
			{
				writer = new FileWriter(failedUserIdepath,true);
				if(missingids.size()>0)
				{
					writer.write("\r\n");
					for(String ids:missingids)
					{
						writer.write(ids+"\r\n");
					}
				}
			}

			writer.close();
			
			/*sevenuserids.removeAll(eightuserids);
			missingids = sevenuserids;*/
			
			System.out.println("3 Completed "+System.currentTimeMillis());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}


