package userMigrationEmailPersonalization;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class ExtractUserIdsFromEmailFile {

	public static void main(String[] args) {
		
		try
		{
			String folderPath = "D:\\Migration\\EmailPersonalization\\Latest DB Ids";
			String fileName = "Set1";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String sevenfilePath = folderPath+"\\7XEmailRecords\\"+fileName+".txt";
			ArrayList<String> userIds = new ArrayList<String>(10000000);
			
			System.out.println("----------------------------- 1 ------------------------");
			
			String file1Id = null;
			
			BufferedReader file1Reader = new BufferedReader(new FileReader(sevenfilePath));
			
			File userFailCreatedForSevenX = new File(folderPath+"\\userIdsFailedDuringCreation.txt");
			FileWriter writer = new FileWriter(userFailCreatedForSevenX);
			
			int j=0;
			while((file1Id = file1Reader.readLine())!=null)
			{
				int currentCount = j%5000;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				
				String userIdds = null;
				String[] tempval = file1Id.split("\t");
				if(tempval.length>4)
				{
					userIdds = tempval[2];
				}
				else
				{
					if(userFailCreatedForSevenX.createNewFile())
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write(file1Id+"\r\n");
					}
					else
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write("\r\n");
						writer.write(file1Id+"\r\n");
					}
				}
				
				userIds.add(userIdds);
				j++;
			}
			file1Reader.close();
			writer.close();
			
			System.out.println("User Ids Size : " + userIds.size());
			
			System.out.println("1 Completed "+System.currentTimeMillis());
			
			System.out.println("----------------------------- 2 ------------------------");
			
			File sevenXIdsPath = new File(folderPath+"\\UserIds.txt");
			
			if(sevenXIdsPath.createNewFile())
			{
				writer = new FileWriter(sevenXIdsPath,true);
				for(String ids:userIds)
				{
					writer.write(ids+"\r\n");
				}
			}
			else
			{
				writer = new FileWriter(sevenXIdsPath,true);
				if(userIds.size()>0)
				{
					writer.write("\r\n");
					for(String ids:userIds)
					{
						writer.write(ids+"\r\n");
					}
				}
			}

			writer.close();
			
			/*sevenuserids.removeAll(eightuserids);
			missingids = sevenuserids;*/
			
			System.out.println("2 Completed "+System.currentTimeMillis());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}


