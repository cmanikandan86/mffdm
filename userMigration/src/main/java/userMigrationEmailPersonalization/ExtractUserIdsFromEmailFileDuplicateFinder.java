package userMigrationEmailPersonalization;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ExtractUserIdsFromEmailFileDuplicateFinder {

	public static void main(String[] args) {
		
		try
		{
			String folderPath = "D:\\Migration\\EmailPersonalization";
			String fileName = "Set1";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String sevenfilePath = folderPath+"\\7XEmailRecords\\"+fileName+".txt";
			String eightfilePath = folderPath+"\\8XEmailRecords\\"+fileName+".txt";
			ArrayList<String> sevenXIds = new ArrayList<String>(10000000);
			ArrayList<String> eightXIds = new ArrayList<String>(10000000);
			
			int maxLimit = 0;
			int recordsToLimit = 500000;
			System.out.println("----------------------------- 1 ------------------------");
			
			String file1Id = null;
			String file2Id = null;
			
			BufferedReader file1Reader = new BufferedReader(new FileReader(sevenfilePath));
			
			File userFailCreatedForSevenX = new File(folderPath+"\\userIdsFailedDuringCreationForSevenX.txt");
			FileWriter writer = new FileWriter(userFailCreatedForSevenX);
			
			int j=0;
			while((file1Id = file1Reader.readLine())!=null)
			{
				if(maxLimit>recordsToLimit)
					break;
				int currentCount = j%5000;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				String khid = null, userId = null, recentTags = null, emailPersonalizationFlag = null;
				String[] tempval = file1Id.split("\t");
				if(tempval.length>4)
				{
					khid = tempval[1];
					userId = tempval[2];
					emailPersonalizationFlag = tempval[3];
					recentTags = tempval[tempval.length-1];
				}
				else
				{
					if(userFailCreatedForSevenX.createNewFile())
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write(file1Id+"\r\n");
					}
					else
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write("\r\n");
						writer.write(file1Id+"\r\n");
					}
				}
				
				sevenXIds.add(userId);
				j++;
				maxLimit++;
				//sevenuserids.add(file1Id);
			}
			file1Reader.close();
			writer.close();
			
			j=0;
			for(int i = 0;i<sevenXIds.size();i++)
			{
				int currentCount = j%250;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				for(int k = i+1; k<sevenXIds.size(); k++)
				{
					if(sevenXIds.get(i).equalsIgnoreCase(sevenXIds.get(k)))
					{
						System.out.println(sevenXIds.get(i) + " Duplicate Records");
					}
					else
					{
						//System.out.println(" No Duplicate Records");
					}
				}
				
				j++;
			}
			
			System.out.println("4 Completed "+System.currentTimeMillis());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}


