package userMigrationKHIDValidation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class DuplicateIdFinder {

	public static void main(String[] args) {
		
		try
		{
			String folderPath = "D:\\Migration\\KHIDS";
			String fileName = "UserIdWithKHID";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String filePath = folderPath+"\\8x Users KHID\\"+fileName+".txt";
			
			System.out.println("----------------------------- 1 ------------------------");
			
			String file2Id = null;
			
			Map<String, String> completeUserDetails = new TreeMap<String, String>();
			Map<Integer, String> duplicateIds = new TreeMap<Integer, String>();
			
			int j=0, duplicateCounter = 0;
			
			System.out.println("1 Completed "+System.currentTimeMillis());
			System.out.println("----------------------------- 2 ------------------------");
			
			File failedUserIdepath = new File(folderPath+"\\userIdsFailedDuringCreation.txt");
			FileWriter writer = new FileWriter(failedUserIdepath,true);
			
			j=0;
			BufferedReader file2Reader = new BufferedReader(new FileReader(filePath));
			while((file2Id = file2Reader.readLine())!=null)
			{
				int currentCount = j%5000;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				else
				{
					//System.out.println(j);
				}
				
				String userId = null;//, eightxkhid = null;
				String[] temp8xval = file2Id.split("\t");
				if(temp8xval.length>2)
				{
					userId = temp8xval[0];
					//eightxkhid = temp8xval[2];
				}
				else
				{
					if(failedUserIdepath.createNewFile())
					{
						writer.write(file2Id+"\r\n");
					}
					else
					{
						writer.write("\r\n");
						writer.write(file2Id+"\r\n");
					}
				}
				
				if(completeUserDetails.containsKey(userId))
				{
					duplicateCounter++;
					duplicateIds.put(duplicateCounter, userId);
				}
				else
				{
					completeUserDetails.put(userId, "");
				}
				
				j++;
			}
			file2Reader.close();
			writer.close();
			
			System.out.println("2 Completed "+System.currentTimeMillis());
			
			System.out.println(duplicateIds.size());
			
			System.out.println("User Id Count: " +j);
			
			System.out.println("----------------------------- 3 ------------------------");
			
			File duplicateIdsFromFile = new File(folderPath+"\\duplicatedIdsFromFile.txt");
			if(duplicateIdsFromFile.createNewFile())
			{
				writer = new FileWriter(duplicateIdsFromFile,true);
				for(Entry<Integer, String> indivVal : duplicateIds.entrySet())
				{
					writer.write(indivVal.getValue()+"\r\n");
				}
			}
			else
			{
				writer = new FileWriter(duplicateIdsFromFile,true);
				for(Entry<Integer, String> indivVal : duplicateIds.entrySet())
				{
					writer.write(indivVal.getValue()+"\r\n");
				}
			}
			
			writer.close();
			System.out.println("3 Completed "+System.currentTimeMillis());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}


