package userMigrationKHIDValidation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class UserKHIDMissingIdsValidation 
{
	public static void main(String[] args) {
		
		try
		{
			String folderPath = "D:\\Migration\\KHIDS";
			String fileName = "UserId";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String sevenfilePath = folderPath+"\\7x Users KHID\\"+fileName+".txt";
			String eightfilePath = folderPath+"\\8x Users KHID\\"+fileName+".txt";
			
			Map<String, String> masterFileIds = new TreeMap<String, String>();
			
			File missingfilepath = new File(folderPath+"\\MissingIds.txt");
			
			System.out.println("----------------------------- 1 ------------------------");

			String file1Id = null;
			String file2Id = null;
			
			BufferedReader file1Reader = new BufferedReader(new FileReader(sevenfilePath));
			BufferedReader file2Reader = new BufferedReader(new FileReader(eightfilePath));
			ArrayList<String> missingids = new ArrayList<String>(10000000);
			
			int j=0;
			File userFailCreatedForEightX = new File(folderPath+"\\userIdsFailedDuringCreationForEightX.txt");
			FileWriter writer = new FileWriter(userFailCreatedForEightX);
			
			while((file2Id = file2Reader.readLine())!=null)
			{
				int currentCount = j%5000;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				
				String eightxuserId = null;
				String[] temp8xval = file2Id.split("\t");
				if(temp8xval.length>0)
				{
					eightxuserId = temp8xval[0];
				}
				else
				{
					if(userFailCreatedForEightX.createNewFile())
					{
						writer = new FileWriter(userFailCreatedForEightX,true);
						writer.write(file2Id+"\r\n");
					}
					else
					{
						writer = new FileWriter(userFailCreatedForEightX,true);
						writer.write("\r\n");
						writer.write(file2Id+"\r\n");
					}
					writer.close();
				}
				
				masterFileIds.put(eightxuserId, "");
				
				j++;
			}
			
			file2Reader.close();
			writer.close();
			System.out.println("1 Completed "+System.currentTimeMillis());
			
			System.out.println("----------------------------- 2 ------------------------");
			
			File userFailCreatedForSevenX = new File(folderPath+"\\userIdsFailedDuringCreationForSevenX.txt");
			writer = new FileWriter(userFailCreatedForSevenX,true);
			
			boolean idFound = false;
			j=0;
			while((file1Id = file1Reader.readLine())!=null)
			{
				idFound = false;
				int currentCount = j%5000;
				//counter = 0;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				
				String sevenxuserId = null;
				String[] tempval = file1Id.split("\t");
				if(tempval.length>0)
				{
					sevenxuserId = tempval[0];
				}
				else
				{
					if(userFailCreatedForSevenX.createNewFile())
					{
						writer.write(file1Id+"\r\n");
					}
					else
					{
						writer.write("\r\n");
						writer.write(file1Id+"\r\n");
					}
				}
				
				if(masterFileIds.containsKey(sevenxuserId))
				{
					idFound = true;
				}
				else if(!idFound)
				{
					missingids.add(file1Id);
				}
				
				j++;
			}
		
			file1Reader.close();
			writer.close();
			
			System.out.println("User Ids Size: " + missingids.size());
			
			System.out.println("2 Completed "+System.currentTimeMillis());
			
			System.out.println("----------------------------- 3 ------------------------");
			
			if(missingfilepath.createNewFile())
			{
				writer = new FileWriter(missingfilepath,true);
				for(String ids:missingids)
				{
					writer.write(ids+"\r\n");
				}
			}
			else
			{
				writer = new FileWriter(missingfilepath,true);
				if(missingids.size()>0)
				{
					writer.write("\r\n");
					for(String ids:missingids)
					{
						writer.write(ids+"\r\n");
					}
				}
			}

			writer.close();
			
			System.out.println("3 Completed "+System.currentTimeMillis());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}


