package userMigrationUserPropertiesV2Validation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;

public class ExtractUserIdsFromEmailFile {

	public static void main(String[] args) {
		
		try
		{
			String folderPath = "D:\\Migration\\UserPropertiesV2";
			String fileName = "UserIdV2Properties";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String sevenfilePath = folderPath+"\\8x Users V2\\"+fileName+".txt";
			ArrayList<String> userIds = new ArrayList<String>(20000000);
			
			System.out.println("----------------------------- 1 ------------------------");
			
			String file1Id = null;
			
			BufferedReader file1Reader = new BufferedReader(new FileReader(sevenfilePath));
			
			File userFailCreatedForSevenX = new File(folderPath+"\\userIdsFailedDuringCreation.txt");
			FileWriter writer = new FileWriter(userFailCreatedForSevenX);
			
			int j=0;
			while((file1Id = file1Reader.readLine())!=null)
			{
				int currentCount = j%5000;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				String userId = null;
				String[] tempval = file1Id.split("\t");
				if(tempval.length>2)
				{
					userId = tempval[0];
				}
				else
				{
					if(userFailCreatedForSevenX.createNewFile())
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write(file1Id+"\r\n");
					}
					else
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write("\r\n");
						writer.write(file1Id+"\r\n");
					}
				}
				
				userIds.add(userId);
				j++;
				//maxLimit++;
				//sevenuserids.add(file1Id);
			}
			file1Reader.close();
			writer.close();
			
			System.out.println("User Ids Size : " + userIds.size());
			
			System.out.println("1 Completed "+System.currentTimeMillis());
			System.out.println("----------------------------- 2 ------------------------");
			
			File sevenXIdsPath = new File(folderPath+"\\UserIdsList.txt");
			
			Collections.sort(userIds);
			
			if(sevenXIdsPath.createNewFile())
			{
				writer = new FileWriter(sevenXIdsPath,true);
				for(String ids:userIds)
				{
					writer.write(ids+"\r\n");
				}
			}
			else
			{
				writer = new FileWriter(sevenXIdsPath,true);
				if(userIds.size()>0)
				{
					writer.write("\r\n");
					for(String ids:userIds)
					{
						writer.write(ids+"\r\n");
					}
				}
			}

			writer.close();
			
			System.out.println("2 Completed "+System.currentTimeMillis());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}


