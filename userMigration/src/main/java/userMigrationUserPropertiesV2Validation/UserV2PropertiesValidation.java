package userMigrationUserPropertiesV2Validation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Map;
import java.util.TreeMap;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class UserV2PropertiesValidation 
{
	
	private static final String AES_GCM_ALGORITHM = "AES/GCM/NoPadding";
    private static final String ALGORITHM_PROVIDER = "SunJCE";
    private static SecretKey secretKey;
    public static final String SKAVA_GCM_TAG_KEY = "skavagcmtagkey";
    public static final String SKAVA_AAD_TAG_KEY = "skavaaadtagkey";
    public static final String SKAVA_USER_SECRET_KEY = "skavaUserSecretKey";
    private static GCMParameterSpec gcmParameterSpec;
    private static byte[] aadKeyInBytes;
    public static final int GCM_NONCE_LENGTH = 12; // in bytes
    public static final int GCM_TAG_LENGTH = 16; // in bytes
    public static final int PASSWORD_HASH_WORKLOAD = 12;
    public static final int KEY_BITS = 16;
    
    private static String[] pdnKey = { "lNb5np3O7er2P0l5", "f4Hi56gKGS+qOYZAoaDbMA==", "8L04C7q7mIY3pQ9qV11clQ==" };
    private static String[] localKey = {"4IYJbmYoRAkPXt0s", "Yx0RDvXPEXbrxvvMkapoXQ==", "Jc3G7ND19M6I84u75l4J0A=="};
    
    static
    {
        secretKey = getKeySpec(SKAVA_USER_SECRET_KEY);
        gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, getKeyInBytes(SKAVA_GCM_TAG_KEY));
        aadKeyInBytes = getKeyInBytes(SKAVA_AAD_TAG_KEY);
    }

	public static void main(String[] args) {
		
		try
		{
			String folderPath = "D:\\Migration\\UserPropertiesV2";
			String fileName = "UserIdV2Properties";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String sevenfilePath = folderPath+"\\7x Users V2\\"+fileName+".txt";
			String eightfilePath = folderPath+"\\8x Users V2\\"+fileName+".txt";
			
			Map<String, String> masterFileIds = new TreeMap<String, String>();
			
			File missingfilepath = new File(folderPath+"\\MissingIds.txt");
			
			System.out.println("----------------------------- 1 ------------------------");

			String file1Id = null;
			String file2Id = null;
			
			BufferedReader file1Reader = new BufferedReader(new FileReader(sevenfilePath));
			BufferedReader file2Reader = new BufferedReader(new FileReader(eightfilePath));
			//ArrayList<String> sevenuserids = new ArrayList<String>(10000000);
			//ArrayList<String> eightuserids = new ArrayList<String>(10000000);
			ArrayList<String> missingids = new ArrayList<String>(10000000);
			
			int j=0;
			File userFailCreatedForEightX = new File(folderPath+"\\userIdsFailedDuringCreationForEightX.txt");
			FileWriter writer = new FileWriter(userFailCreatedForEightX);
			
			while((file2Id = file2Reader.readLine())!=null)
			{
				int currentCount = j%5000;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				
				String eightxuserId = null, eightxkhid = null;
				String[] temp8xval = file2Id.split("\t");
				if(temp8xval.length>2)
				{
					eightxuserId = temp8xval[0];
					eightxkhid = temp8xval[2];
				}
				else
				{
					if(userFailCreatedForEightX.createNewFile())
					{
						writer = new FileWriter(userFailCreatedForEightX,true);
						writer.write(file2Id+"\r\n");
					}
					else
					{
						writer = new FileWriter(userFailCreatedForEightX,true);
						writer.write("\r\n");
						writer.write(file2Id+"\r\n");
					}
					writer.close();
				}
				
				masterFileIds.put(eightxuserId, eightxkhid);
				
				j++;
			}
			
			file2Reader.close();
			writer.close();
			
			System.out.println("----------------------------- 1 ------------------------");
			
			File userFailCreatedForSevenX = new File(folderPath+"\\userIdsFailedDuringCreationForSevenX.txt");
			writer = new FileWriter(userFailCreatedForSevenX);
			File mismatchedKhidsfilepath = new File(folderPath+"\\MismatchedUserSubscriptionV2Details.txt");
			FileWriter writer1 = new FileWriter(mismatchedKhidsfilepath,true);
			
			boolean idFound = false;
			j=0;
			//int recordsToLimit = 500000;
			while((file1Id = file1Reader.readLine())!=null)
			{
				idFound = false;
				//if(maxLimit>recordsToLimit)
					//break;
				int currentCount = j%5000;
				//counter = 0;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				
				String sevenxuserId = null, sevenxkhid = null;
				String[] tempval = file1Id.split("\t");
				if(tempval.length>2)
				{
					sevenxuserId = tempval[0];
					sevenxkhid = tempval[2];
				}
				else
				{
					if(userFailCreatedForSevenX.createNewFile())
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write(file1Id+"\r\n");
					}
					else
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write("\r\n");
						writer.write(file1Id+"\r\n");
					}
					writer.close();
				}
				
				//int subCounter = 0;
				if(masterFileIds.containsKey(sevenxuserId))
				{
					idFound = true;
					String value = decrypt(sevenxkhid);
					String eightxkhid = masterFileIds.get(sevenxuserId);
					if((value!=null)&&(value.equalsIgnoreCase(eightxkhid)))
		            {
		            	//System.out.println("Pass "+sevenxuserId);
		            }
					else if(value==null)
					{
						System.out.println("Fail Due to Null "+sevenxuserId);
					}
					else
		            {
		            	System.out.println("Fail "+sevenxuserId);
		            	if(mismatchedKhidsfilepath.createNewFile())
						{
		            		writer1.write(sevenxuserId+"\r\n");
		            		writer1.write("==========="+"\r\n");
		            		writer1.write("Encrypted Expected Seven X Subscription Values : "+ sevenxkhid+"\n");
		            		writer1.write("Decrypted Expected Seven X Subscription Values : "+ value+"\n");
		            		writer1.write("Actual Eight X Subscription Values : "+ eightxkhid+"\t\n");
		            		writer1.write("==========="+"\r\n");
						}
						else
						{
							writer1.write(sevenxuserId+"\r\n");
							writer1.write("==========="+"\r\n");
							writer1.write("Encrypted Seven X Subscription Values : "+ sevenxkhid+"\n");
							writer1.write("Decrypted Expected Seven X Subscription Values : "+ value+"\n");
							writer1.write("Actual Eight X Subscription Values : "+ eightxkhid+"\t\n");
							writer1.write("==========="+"\r\n");
						}
		            }
				}
				
				if(!idFound)
				{
					//counter = 0;
					missingids.add(sevenxuserId);
					//file1Reader.close();
					//file1Reader = new BufferedReader(new FileReader(sevenfilePath));
				}
				
				j++;
				//mainCounter++;
			}
			file1Reader.close();
			file2Reader.close();
			writer1.close();
			
			System.out.println("1 Completed "+System.currentTimeMillis());
			
			System.out.println("----------------------------- 2 ------------------------");
			
			if(missingfilepath.createNewFile())
			{
				writer = new FileWriter(missingfilepath,true);
				for(String ids:missingids)
				{
					writer.write(ids+"\r\n");
				}
			}
			else
			{
				writer = new FileWriter(missingfilepath,true);
				if(missingids.size()>0)
				{
					writer.write("\r\n");
					for(String ids:missingids)
					{
						writer.write(ids+"\r\n");
					}
				}
			}

			writer.close();
			
			System.out.println("2 Completed "+System.currentTimeMillis());
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static String decrypt(String data)
    {
        String toRet = null;

        try
        {
            Cipher decryptCipher = Cipher.getInstance(AES_GCM_ALGORITHM, ALGORITHM_PROVIDER);
            decryptCipher.init(Cipher.DECRYPT_MODE, secretKey, gcmParameterSpec);
            decryptCipher.updateAAD(aadKeyInBytes);
            byte[] decrtptedData = decryptCipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(data));
            toRet = new String(decrtptedData, "UTF-8");
        }
        catch (Exception e)
        {
            System.out.println("Error while decrypting : " + data);
        }

        return toRet;
    } 
	
	public static SecretKeySpec getKeySpec(String alias)
    {
        SecretKeySpec secretKey = null;
        byte[] keyInBytes = getKeyInBytes(alias);

        if (keyInBytes != null)
        {
            secretKey = new SecretKeySpec(keyInBytes, 0, keyInBytes.length, "AES");
        }

        return secretKey;
    }

    public static byte[] getKeyInBytes(String alias)
    {
        byte[] toRet = null;
        String keyStr = getKeyfromAPI(alias);
        if (keyStr != null)
        {
            toRet = Base64.getDecoder().decode(keyStr);
        }
        return toRet;
    }
    
    public static String getKeyfromAPI(String keyStoreName)
    {
        String builder = null;
        try
        {
            if (keyStoreName.equals(SKAVA_GCM_TAG_KEY))
            {
                builder = pdnKey[0];
            }
            else if (keyStoreName.equals(SKAVA_AAD_TAG_KEY))
            {
                builder = pdnKey[1];

            }
            else if (keyStoreName.equals(SKAVA_USER_SECRET_KEY))
            {
                builder = pdnKey[2];
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return builder;
    }

}


