package userMigrationUserPropertiesV2Validation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

public class DuplicateIdFinder {

	public static void main(String[] args) {
		
		try
		{
			String folderPath = "D:\\Migration\\UserPropertiesV2";
			String fileName = "UserIdsList";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String filePath = folderPath+"\\8x Users V2\\"+fileName+".txt";
			
			System.out.println("----------------------------- 1 ------------------------");
			
			String file2Id = null;
			
			Map<String, String> completeUserDetails = new TreeMap<String, String>();
			Map<Integer, String> duplicateIds = new TreeMap<Integer, String>();
			
			int j=0, duplicateCounter = 0;
			
			System.out.println("1 Completed "+System.currentTimeMillis());
			System.out.println("----------------------------- 2 ------------------------");
			
			File userFailCreatedForEightX = new File(folderPath+"\\userIdsFailedDuringCreation.txt");
			FileWriter writer = new FileWriter(userFailCreatedForEightX,true);
			
			j=0;
			BufferedReader file2Reader = new BufferedReader(new FileReader(filePath));
			while((file2Id = file2Reader.readLine())!=null)
			{
//				if(maxLimit>100)
//					break;
				int currentCount = j%5000;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				else
				{
					//System.out.println(j);
				}
				
				String userId = null;//, eightxkhid = null;
				String[] temp8xval = file2Id.split("\t");
				if(temp8xval.length==1)
				{
					userId = temp8xval[0];
					//eightxkhid = temp8xval[2];
				}
				else
				{
					if(userFailCreatedForEightX.createNewFile())
					{
						writer.write(file2Id+"\r\n");
					}
					else
					{
						writer.write("\r\n");
						writer.write(file2Id+"\r\n");
					}
				}
				
				if(completeUserDetails.containsKey(userId))
				{
					duplicateCounter++;
					duplicateIds.put(duplicateCounter, userId);
				}
				else
				{
					completeUserDetails.put(userId, "");
				}
				
				j++;
			}
			file2Reader.close();
			writer.close();
			
			System.out.println("2 Completed "+System.currentTimeMillis());
			
			System.out.println(duplicateIds.size());
			
			System.out.println("User Id Count: " +j);
			
			System.out.println("----------------------------- 3 ------------------------");
			
			File duplicateIdsFromFile = new File(folderPath+"\\duplicatedIdsFromFile.txt");
			if(duplicateIdsFromFile.createNewFile())
			{
				writer = new FileWriter(duplicateIdsFromFile,true);
				for(Entry<Integer, String> indivVal : duplicateIds.entrySet())
				{
					writer.write(indivVal.getValue()+"\r\n");
				}
			}
			else
			{
				writer = new FileWriter(duplicateIdsFromFile,true);
				for(Entry<Integer, String> indivVal : duplicateIds.entrySet())
				{
					writer.write(indivVal.getKey() + " -  " + indivVal.getValue()+"\r\n");
				}
			}
			
			writer.close();
			System.out.println("3 Completed "+System.currentTimeMillis());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}


