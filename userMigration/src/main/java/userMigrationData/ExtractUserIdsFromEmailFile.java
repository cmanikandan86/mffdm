package userMigrationData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class ExtractUserIdsFromEmailFile {

	public static void main(String[] args) {
		
		try
		{
			String folderPath = "D:\\Migration\\KHIDS";
			String fileName = "UserIdWithKHID1";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String sevenfilePath = folderPath+"\\7x Users KHID\\"+fileName+".txt";
			ArrayList<String> sevenXIds = new ArrayList<String>(10000000);
			
			int maxLimit = 0;
			//int recordsToLimit = 500000;
			System.out.println("----------------------------- 1 ------------------------");
			
			String file1Id = null;
			String file2Id = null;
			
			BufferedReader file1Reader = new BufferedReader(new FileReader(sevenfilePath));
			
			File userFailCreatedForSevenX = new File(folderPath+"\\userIdsFailedDuringCreationForSevenX.txt");
			FileWriter writer = new FileWriter(userFailCreatedForSevenX);
			
			int j=0;
			while((file1Id = file1Reader.readLine())!=null)
			{
				//if(maxLimit>recordsToLimit)
					//break;
				int currentCount = j%5000;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				
				String sevenxuserId = null, sevenxkhid = null;
				String[] tempval = file1Id.split("\t");
				if(tempval.length>4)
				{
					sevenxuserId = tempval[0];
					sevenxkhid = tempval[2];
				}
				else
				{
					if(userFailCreatedForSevenX.createNewFile())
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write(file1Id+"\r\n");
					}
					else
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write("\r\n");
						writer.write(file1Id+"\r\n");
					}
				}
				
				sevenXIds.add(sevenxuserId);
				j++;
				maxLimit++;
				//sevenuserids.add(file1Id);
			}
			file1Reader.close();
			writer.close();
			
			System.out.println(sevenXIds.size());
			
			System.out.println("1 Completed "+System.currentTimeMillis());
			System.out.println("----------------------------- 2 ------------------------");
			
			System.out.println("----------------------------- 3 ------------------------");
			
			File sevenXIdsPath = new File(folderPath+"\\SevenXUserIdsList.txt");
			
			if(sevenXIdsPath.createNewFile())
			{
				writer = new FileWriter(sevenXIdsPath,true);
				for(String ids:sevenXIds)
				{
					writer.write(ids+"\r\n");
				}
			}
			else
			{
				writer = new FileWriter(sevenXIdsPath,true);
				if(sevenXIds.size()>0)
				{
					writer.write("\r\n");
					for(String ids:sevenXIds)
					{
						writer.write(ids+"\r\n");
					}
				}
			}

			writer.close();
			
			/*sevenuserids.removeAll(eightuserids);
			missingids = sevenuserids;*/
			
			System.out.println("3 Completed "+System.currentTimeMillis());
			
			System.out.println("----------------------------- 4 ------------------------");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}


