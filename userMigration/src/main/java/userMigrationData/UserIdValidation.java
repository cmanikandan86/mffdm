package userMigrationData;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class UserIdValidation {

	public static void main(String[] args) {
		
		try
		{
			
			long timestamp = System.currentTimeMillis();
			String folderPath = "D:\\Migration";
			String fileName = "UserFile1";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String sevenfilePath = folderPath+"\\sevenuserids\\UserId.txt";
			String eightfilePath = folderPath+"\\eightuserids\\"+fileName+".txt";
//			File missingfilepath = new File(folderPath+"\\MissingIds_"+timestamp+".txt");
			File missingfilepath = new File(folderPath+"\\MissingIds.txt");
			ArrayList<String> sevenuserids = new ArrayList<String>(30000000);
			ArrayList<String> eightuserids = new ArrayList<String>(30000000);
			ArrayList<String> missingids = new ArrayList<String>(30000000);
			
			Scanner scan = new Scanner(new File(sevenfilePath));
			
			int i=0;
			System.out.println("----------------------------- 1 ------------------------");
			while(scan.hasNext()) {
//				i++;
				sevenuserids.add(scan.nextLine());
//				if(i>1000)
//				break;
				}
			scan = new Scanner(new File(eightfilePath));
			System.out.println("----------------------------- 2 ------------------------");
			while(scan.hasNext()) {
				i++;
				eightuserids.add(scan.nextLine());
				if(i>1000000)
				break;
				}
			System.out.println("----------------------------- 3 ------------------------");
			int j=0;
			for(String ids:eightuserids) {
				int currentCount = j%5000;
				if(currentCount == 0) {
				System.out.println(j);	
				
				}
				
				if(!sevenuserids.contains(ids))
				{
					missingids.add(ids);	
				}
				j++;
			}
			System.out.println("----------------------------- 4 ------------------------");
			System.out.println(sevenuserids.size());
			System.out.println(eightuserids.size());
			System.out.println(missingids.size());
//			System.out.println(sevenuserids);
			FileWriter writer = new FileWriter(missingfilepath);
			writer.write("Users from seven x DB : "+sevenuserids.size()+"\r\n");
			writer.write("Users from eight x DB : "+eightuserids.size()+"\r\n");
			
			writer.write("Missing Ids size :"+ missingids.size()+"\r\n");
			for(String ids:missingids)
			{
				writer.write(ids+"\r\n");
			}	
			
			writer.close();
			
			System.out.println("Scripts Ended: "+System.currentTimeMillis());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}
		
		

	}

}


