package userMigrationData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class EmaiMissingUserIdValidationWith2BF {

	public static void main(String[] args) {
		
		try
		{
			String folderPath = "D:\\Migration\\EmailPersonalization\\SFTPAgainstDB";
			String fileName = "Set1";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String sevenfilePath = folderPath+"\\SevenX\\"+fileName+".txt";
			String eightfilePath = folderPath+"\\EightX\\"+fileName+".txt";
			
			File missingfilepath = new File(folderPath+"\\MissingIds.txt");
			
			System.out.println("----------------------------- 1 ------------------------");

			String file1Id = null;
			String file2Id = null;
			
			BufferedReader file1Reader = new BufferedReader(new FileReader(sevenfilePath));
			BufferedReader file2Reader = new BufferedReader(new FileReader(eightfilePath));
			//ArrayList<String> sevenuserids = new ArrayList<String>(10000000);
			//ArrayList<String> eightuserids = new ArrayList<String>(10000000);
			ArrayList<String> missingids = new ArrayList<String>(10000000);
			int j=0;
			int mainCounter = 0, counter = 0;
			boolean idFound = false;
			while((file1Id = file1Reader.readLine())!=null)
			{
				idFound = false;
				int currentCount = j%5000;
				counter = 0;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				
				int subCounter = 0;
				while((file2Id = file2Reader.readLine())!=null)
				{
					subCounter++;
					if(file1Id.equalsIgnoreCase(file2Id))
					{
						idFound = true;
						break;
					}
					else
					{
						int tmpCounter = mainCounter+500000;
						if(tmpCounter < subCounter)
						{
							break;
						}
						//missingids.add(file2Id);
					}
					
					counter++;
				}
				
				if(!idFound)
				{
					counter = 0;
					missingids.add(file1Id);
					file2Reader.close();
					file2Reader = new BufferedReader(new FileReader(eightfilePath));
				}
				
				j++;
				mainCounter++;
			}
			file1Reader.close();
			file2Reader.close();
			
			FileWriter writer;
			
			if(missingfilepath.createNewFile())
			{
				writer = new FileWriter(missingfilepath,true);
				for(String ids:missingids)
				{
					writer.write(ids+"\r\n");
				}
			}
			else
			{
				writer = new FileWriter(missingfilepath,true);
				if(missingids.size()>0)
				{
					writer.write("\r\n");
					for(String ids:missingids)
					{
						writer.write(ids+"\r\n");
					}
				}
			}

			writer.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}


