package userMigrationData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class splitUserFiles {

	public static void main(String[] args) {
		
		try
		{
			long recordsToSplit = 160000;
			String folderPath = "D:\\Migration\\eightuserids\\";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String eightfilePath = folderPath+"Users_file.txt";
			ArrayList<String> userIds = new ArrayList<String>(30000000);
			System.out.println("----------------------------- 1 ------------------------");
			String file1Id = null;
			BufferedReader file1Reader = new BufferedReader(new FileReader(eightfilePath));
			int i = 1;
			int startingRecords = 22070000;
			while((file1Id = file1Reader.readLine())!=null)
			{
				if(i>24000000)
					break;
				
				if(i>startingRecords)
				{
					userIds.add(file1Id);
				}
				i++;
			}
			file1Reader.close();
			
			System.out.println("1 Completed "+System.currentTimeMillis());
			System.out.println("----------------------------- 2 ------------------------");
			System.out.println("User Ids Size : " + userIds.size());
			long fileSplitSize = userIds.size()/recordsToSplit;
			long remainingFile = userIds.size()%recordsToSplit;
			if(remainingFile>0)
			{
				fileSplitSize++;
			}
			System.out.println("Total files to generate : " + fileSplitSize);
			int startingValue = 0;
			int iterationCount = 0;
			int tempFileName = 222;
			for(int j = 1; j<=fileSplitSize; j++)
			{
				long counter = 1;
				File userCreatedFile = new File(folderPath+"UserFile"+(tempFileName)+".txt");
				FileWriter writer = new FileWriter(userCreatedFile);
				//ArrayList<String> userIdToWrite = new ArrayList<String>();
				for(int k=startingValue; k<userIds.size();k++)
				{
					startingValue++;
					writer.write(userIds.get(k)+"\r\n");
					//userIdToWrite.add(userIds.get(k));
					if(iterationCount==0)
					{
						if(counter==recordsToSplit)
						{
							break;
						}
						counter++;
					}
					else
					{
						if(counter==recordsToSplit+0)
						{
							break;
						}
						counter++;
					}
				}
				//writer.write(userIdToWrite.toString());
				writer.close();
				startingValue = startingValue-60000;
				iterationCount++;
				tempFileName++;
				//break;
			}
			
			System.out.println("2 Completed "+System.currentTimeMillis());
			System.out.println("----------------------------- 3 ------------------------");
			//System.out.println("Seven Ids Size : " + sevenIds.size());
			System.out.println("Scripts Ended: "+System.currentTimeMillis());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}


