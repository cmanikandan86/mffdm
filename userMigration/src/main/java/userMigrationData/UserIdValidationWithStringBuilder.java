package userMigrationData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class UserIdValidationWithStringBuilder {

	public static void main(String[] args) {
		
		try
		{
			String folderPath = "D:\\Migration";
			String fileName = "UserId";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String sevenfilePath = folderPath+"\\sevenuserids\\"+fileName+".txt";
			String eightfilePath = folderPath+"\\eightuserids\\"+fileName+".txt";
			
			File missingfilepath = new File(folderPath+"\\MissingIds.txt");
			
			System.out.println("----------------------------- 1 ------------------------");

			String file1Id = null;
			String file2Id = null;
			
			BufferedReader file1Reader = new BufferedReader(new FileReader(sevenfilePath));
			ArrayList<String> sevenuserids = new ArrayList<String>(10000000);
			ArrayList<String> eightuserids = new ArrayList<String>(10000000);
			ArrayList<String> missingids = new ArrayList<String>(10000000);
			String sevenxUserIdsString = "";
			int i = 0;
			while((file1Id = file1Reader.readLine())!=null)
			{
				sevenuserids.add(file1Id);
			}
			sevenxUserIdsString = sevenuserids.toString();
			System.out.println(sevenxUserIdsString);
			file1Reader.close();
			
			System.out.println("1 Completed "+System.currentTimeMillis());
			System.out.println("----------------------------- 2 ------------------------");
	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}


