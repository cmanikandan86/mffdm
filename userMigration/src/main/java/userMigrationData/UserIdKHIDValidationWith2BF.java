package userMigrationData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class UserIdKHIDValidationWith2BF {

	private static final String AES_GCM_ALGORITHM = "AES/GCM/NoPadding";
    private static final String ALGORITHM_PROVIDER = "SunJCE";
    private static SecretKey secretKey;
    public static final String SKAVA_GCM_TAG_KEY = "skavagcmtagkey";
    public static final String SKAVA_AAD_TAG_KEY = "skavaaadtagkey";
    public static final String SKAVA_USER_SECRET_KEY = "skavaUserSecretKey";
    private static GCMParameterSpec gcmParameterSpec;
    private static byte[] aadKeyInBytes;
    public static final int GCM_NONCE_LENGTH = 12; // in bytes
    public static final int GCM_TAG_LENGTH = 16; // in bytes
    public static final int PASSWORD_HASH_WORKLOAD = 12;
    public static final int KEY_BITS = 16;
    
    private static String[] pdnKey = { "lNb5np3O7er2P0l5", "f4Hi56gKGS+qOYZAoaDbMA==", "8L04C7q7mIY3pQ9qV11clQ==" };
    private static String[] localKey = {"4IYJbmYoRAkPXt0s", "Yx0RDvXPEXbrxvvMkapoXQ==", "Jc3G7ND19M6I84u75l4J0A=="};
    
    static
    {
        secretKey = getKeySpec(SKAVA_USER_SECRET_KEY);
        gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, getKeyInBytes(SKAVA_GCM_TAG_KEY));
        aadKeyInBytes = getKeyInBytes(SKAVA_AAD_TAG_KEY);
    }
    
	public static void main(String[] args) 
	{
		
		try
		{
			String folderPath = "D:\\Migration\\KHIDS";
			String fileName = "UserIdWithKHID";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String sevenfilePath = folderPath+"\\7x Users KHID\\"+fileName+".txt";
			String eightfilePath = folderPath+"\\8x Users KHID\\"+fileName+".txt";
			
			File missingfilepath = new File(folderPath+"\\MissingIds.txt");
			File mismatchedKhidsfilepath = new File(folderPath+"\\MismatchedUserKHIDS.txt");
			
			System.out.println("----------------------------- 1 ------------------------");

			String file1Id = null;
			String file2Id = null;
			
			ArrayList<String> missingids = new ArrayList<String>(10000000);
			//HashMap<String, String> khids = new HashMap<String, String>();
			//LinkedHashMap<String, HashMap<String, String>> mismatchedIds = new LinkedHashMap<String, HashMap<String, String>>();
			int j=0;
			int mainCounter = 0;
			boolean idFound = false;
			
			File userFailCreatedForSevenX = new File(folderPath+"\\userIdsFailedDuringCreationForSevenX.txt");
			File userFailCreatedForEightX = new File(folderPath+"\\userIdsFailedDuringCreationForEightX.txt");
			
			FileWriter writer = new FileWriter(userFailCreatedForSevenX);
			
			BufferedReader file1Reader = new BufferedReader(new FileReader(sevenfilePath));
			BufferedReader file2Reader = new BufferedReader(new FileReader(eightfilePath));
			
			while((file1Id = file1Reader.readLine())!=null)
			{
				idFound = false;
				int currentCount = j%5000;
				//counter = 0;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				
				String sevenxuserId = null, sevenxkhid = null;
				String[] tempval = file1Id.split("\t");
				if(tempval.length>2)
				{
					sevenxuserId = tempval[0];
					sevenxkhid = tempval[2];
				}
				else
				{
					if(userFailCreatedForSevenX.createNewFile())
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write(file1Id+"\r\n");
					}
					else
					{
						writer = new FileWriter(userFailCreatedForSevenX,true);
						writer.write("\r\n");
						writer.write(file1Id+"\r\n");
					}
					writer.close();
				}
				
				int subCounter = 0;
				String eightxuserId = null, eightxkhid = null;
				while((file2Id = file2Reader.readLine())!=null)
				{
					subCounter++;
					String[] temp8xval = file2Id.split("\t");
					if(temp8xval.length>2)
					{
						eightxuserId = temp8xval[0];
						eightxkhid = temp8xval[2];
					}
					else
					{
						if(userFailCreatedForEightX.createNewFile())
						{
							writer = new FileWriter(userFailCreatedForEightX,true);
							writer.write(file2Id+"\r\n");
						}
						else
						{
							writer = new FileWriter(userFailCreatedForEightX,true);
							writer.write("\r\n");
							writer.write(file2Id+"\r\n");
						}
						writer.close();
					}
					
					if(sevenxuserId.equalsIgnoreCase(eightxuserId))
					{
						idFound = true;
						break;
					}
					else
					{
						int tmpCounter = mainCounter+100000;
						if(tmpCounter < subCounter)
						{
							break;
						}
						//missingids.add(file2Id);
					}
					
					//counter++;
				}
				
				if(!idFound)
				{
					//counter = 0;
					missingids.add(file1Id);
					file2Reader.close();
					file2Reader = new BufferedReader(new FileReader(eightfilePath));
				}
				else
				{
					String value = decrypt(sevenxkhid);
					//System.out.println(value);
		            if((value!=null)&&(value.equalsIgnoreCase(eightxkhid)))
		            {
		            	System.out.println("Pass");
		            }
		            else
		            {
		            	System.out.println("Fail");
		            	if(mismatchedKhidsfilepath.createNewFile())
						{
							writer = new FileWriter(mismatchedKhidsfilepath,true);
							writer.write(sevenxuserId+"\r\n");
							writer.write("==========="+"\r\n");
							writer.write("Expected Seven X KHID Values : "+ sevenxkhid+"\t");
							writer.write("Actual Eight X KHID Values : "+ eightxkhid+"\t\n");
							writer.write("==========="+"\r\n");
						}
						else
						{
							writer = new FileWriter(mismatchedKhidsfilepath,true);
							writer.write(sevenxuserId+"\r\n");
							writer.write("==========="+"\r\n");
							writer.write("Expected Seven X KHID Values : "+ sevenxkhid+"\t");
							writer.write("Actual Eight X KHID Values : "+ eightxkhid+"\t\n");
							writer.write("==========="+"\r\n");
						}
		            }
				}
				
				j++;
				mainCounter++;
			}
			file1Reader.close();
			file2Reader.close();
			writer.close();
			
			if(missingfilepath.createNewFile())
			{
				writer = new FileWriter(missingfilepath,true);
				for(String ids:missingids)
				{
					writer.write(ids+"\r\n");
				}
			}
			else
			{
				writer = new FileWriter(missingfilepath,true);
				if(missingids.size()>0)
				{
					writer.write("\r\n");
					for(String ids:missingids)
					{
						writer.write(ids+"\r\n");
					}
				}
			}

			writer.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static String decrypt(String data)
    {
        String toRet = null;

        try
        {
            Cipher decryptCipher = Cipher.getInstance(AES_GCM_ALGORITHM, ALGORITHM_PROVIDER);
            decryptCipher.init(Cipher.DECRYPT_MODE, secretKey, gcmParameterSpec);
            decryptCipher.updateAAD(aadKeyInBytes);
            byte[] decrtptedData = decryptCipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(data));
            toRet = new String(decrtptedData, "UTF-8");
        }
        catch (Exception e)
        {
            System.out.println("Error while decrypting : " + data);
        }

        return toRet;
    } 
	
	public static SecretKeySpec getKeySpec(String alias)
    {
        SecretKeySpec secretKey = null;
        byte[] keyInBytes = getKeyInBytes(alias);

        if (keyInBytes != null)
        {
            secretKey = new SecretKeySpec(keyInBytes, 0, keyInBytes.length, "AES");
        }

        return secretKey;
    }

    public static byte[] getKeyInBytes(String alias)
    {
        byte[] toRet = null;
        String keyStr = getKeyfromAPI(alias);
        if (keyStr != null)
        {
            toRet = Base64.getDecoder().decode(keyStr);
        }
        return toRet;
    }
    
    public static String getKeyfromAPI(String keyStoreName)
    {
        String builder = null;
        try
        {
            if (keyStoreName.equals(SKAVA_GCM_TAG_KEY))
            {
                builder = pdnKey[0];
            }
            else if (keyStoreName.equals(SKAVA_AAD_TAG_KEY))
            {
                builder = pdnKey[1];

            }
            else if (keyStoreName.equals(SKAVA_USER_SECRET_KEY))
            {
                builder = pdnKey[2];
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return builder;
    }

}


