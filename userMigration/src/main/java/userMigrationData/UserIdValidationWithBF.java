package userMigrationData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class UserIdValidationWithBF {

	public static void main(String[] args) {
		
		try
		{
			String folderPath = "D:\\Migration";
			String fileName = "UserFile233";
			System.out.println("Scripts Started: "+System.currentTimeMillis());
			String sevenfilePath = folderPath+"\\sevenuserids\\"+fileName+".txt";
			String eightfilePath = folderPath+"\\eightuserids\\"+fileName+".txt";
			
			File missingfilepath = new File(folderPath+"\\MissingIds.txt");
			
			System.out.println("----------------------------- 1 ------------------------");

			String file1Id = null;
			String file2Id = null;
			
			BufferedReader file1Reader = new BufferedReader(new FileReader(sevenfilePath));
			ArrayList<String> sevenuserids = new ArrayList<String>(10000000);
			ArrayList<String> eightuserids = new ArrayList<String>(10000000);
			ArrayList<String> missingids = new ArrayList<String>(10000000);
			while((file1Id = file1Reader.readLine())!=null)
			{
				sevenuserids.add(file1Id);
			}
			file1Reader.close();
			
			System.out.println("1 Completed "+System.currentTimeMillis());
			System.out.println("----------------------------- 2 ------------------------");
			
			BufferedReader file2Reader = new BufferedReader(new FileReader(eightfilePath));
			while((file2Id = file2Reader.readLine())!=null)
			{
				eightuserids.add(file2Id);
			}
			file2Reader.close();
			
			System.out.println("2 Completed "+System.currentTimeMillis());
			
			System.out.println("----------------------------- 3 ------------------------");
			
			int j=0;
			for(String ids:sevenuserids) 
			{
				int currentCount = j%5000;
				if(currentCount == 0) 
				{
					System.out.println(j);	
				}
				
				if(!eightuserids.contains(ids))
				{
					missingids.add(ids);	
				}
				j++;
			}
			
			/*sevenuserids.removeAll(eightuserids);
			missingids = sevenuserids;*/
			
			System.out.println("3 Completed "+System.currentTimeMillis());
			System.out.println("----------------------------- 4 ------------------------");
			
			System.out.println("Seven Ids Size : " + sevenuserids.size());
			System.out.println("Eight Ids Size : " + eightuserids.size());
			System.out.println("Missing Ids size :"+ missingids.size());
			System.out.println("Scripts Ended: "+System.currentTimeMillis());

			long timestamp = System.currentTimeMillis();
			File iteratorResultfilepath = new File(folderPath+"\\IterationReport\\"+fileName+"_"+timestamp+".txt");
			FileWriter writer = new FileWriter(iteratorResultfilepath);
			writer.write("Users from seven x DB : "+sevenuserids.size()+"\r\n");
			writer.write("Users from eight x DB : "+eightuserids.size()+"\r\n");
			writer.write("Starting 7 X User ids : "+sevenuserids.get(0)+"\r\n");
			writer.write("Ending 7 X User ids : "+sevenuserids.get(sevenuserids.size()-1)+"\r\n");
			writer.write("Starting 8 X User ids : "+eightuserids.get(0)+"\r\n");
			writer.write("Ending 8 X User ids : "+eightuserids.get(eightuserids.size()-1)+"\r\n");
			writer.write("Missing Ids size :"+ missingids.size()+"\r\n");
			writer.write("Missing Ids :"+ missingids);
			writer.close();
			
			if(missingfilepath.createNewFile())
			{
				writer = new FileWriter(missingfilepath,true);
				for(String ids:missingids)
				{
					writer.write(ids+"\r\n");
				}
			}
			else
			{
				writer = new FileWriter(missingfilepath,true);
				if(missingids.size()>0)
				{
					writer.write("\r\n");
					for(String ids:missingids)
					{
						writer.write(ids+"\r\n");
					}
				}
			}

			writer.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}


